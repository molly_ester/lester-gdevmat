class Walker{
  float xPos, yPos;
  
  void Render(){
    circle(xPos, yPos, 30);
    noStroke();
    fill(random(255), random(255), random(255), random(255));
  }
  
  void RandomWalk(){
    int decision = floor(random(8));
    if (decision == 0){
      yPos += 2;
    } else if (decision == 1){
      yPos -= 2;
    } else if (decision == 2){
      xPos += 2;
    } else if (decision == 3){
      xPos -= 2;
    } else if (decision == 4){
      xPos += 2;
      yPos += 2;
    } else if (decision == 5){
      xPos -= 2;
      yPos -= 2;
    } else if (decision == 6){
      xPos -= 2;
      yPos += 2;
    } else if (decision == 7){
      xPos += 2;
      yPos -= 2;
    }
      
  }
  
}
