void setup(){
  size (1920, 1080, P3D);
  camera(0, 0, -(height/2.0) / tan(PI * 30.0f / 180.0), 0, 0, 0, 0, -1, 0);
}

Walker walker = new Walker();

void draw(){
  walker.Render();
  walker.RandomWalk();
  
  //int randomValues = ceil(random(1, 100));
  //println(randomValues);
}
