public class Matter{
    public PVector pos = new PVector();
    public PVector vel = new PVector(); 
    float size = floor(random(30, 50));
    //color
    float r = random(0, 255), g  = random(0, 255), b= random(0, 255), a = random(60,200);
  
  public void Render(){
    circle(pos.x, pos.y, size);
    noStroke();
    fill(r,g,b,a);
  }
  
  public void Random(){
    float perlin = random(1, 69);
    
    r = map(noise(perlin * 5), 0, 1, 0, 255);
    g = map(noise(perlin * 10), 0, 1, 0, 255);
    b = map(noise(perlin * 20), 0, 1, 0, 255);
    a = map(noise(perlin * 69), 0, 1, 60, 200);
    
    float standardDeviation=400;
    float mean = 59;
  
    pos.x = (standardDeviation * randomGaussian()) + mean;
    
    standardDeviation = 400;
    mean = 8;
    pos.y = (randomGaussian() * standardDeviation) + mean;
    
    standardDeviation = 50;
    mean = 8;
    size = (randomGaussian() * standardDeviation) + mean;
   
  }
  
  public void Sucked(PVector bHole){
     vel = PVector.sub(bHole, pos).normalize();  
     
     vel.mult(10);
     pos.add(vel);
    
  }
}
