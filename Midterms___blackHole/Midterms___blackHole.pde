PVector bHole;
Matter[] matter;

void setup(){
  size (1920, 1080, P3D);
  camera(0, 0, -(height/2.0) / tan(PI * 30.0f / 180.0), 0, 0, 0, 0, -1, 0);
  background(0.);
  matter = new Matter[100];
  for(int i = 0; i < 100; i++){
     matter[i] = new Matter(); 
  }
}

void draw(){
  clear();
  if (frameCount % 201 == 1){
    RandomSpawn();
  }
  
  simulation();
  fill(255);
}

void RandomSpawn(){
   bHole = new PVector(random(Window.left, Window.right), random(Window.bottom, Window.top));
   blackHoleRender();
   
   
   for(Matter m : matter){
      m.Random();
      m.Render();
   }
}

void blackHoleRender(){
     circle(bHole.x, bHole.y, 50);
   noStroke();
   fill(255);
  
}
void simulation(){
  for(Matter m : matter){
    m.Sucked(bHole); 
    m.Render(); 
  }
  blackHoleRender();
}
