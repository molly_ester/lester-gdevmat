void setup(){
  size (1920, 1080, P3D);
  camera(0, 0, -(height/2.0) / tan(PI * 30.0f / 180.0), 0, 0, 0, 0, -1, 0);
  background(0.);
}

void draw(){
  
  if (frameCount % 1000 == 0) clear();
  float gauss = randomGaussian();
  float standardDeviation=400;
  float mean = 59;
  
  float xPos = (standardDeviation * gauss) + mean;
  
  standardDeviation = 50;
  mean = 8;
  float gauss2 = randomGaussian();
  float size = (gauss2 * standardDeviation) + mean;
  
  
  
  noStroke();
  fill(random(255), random(255),random(255) , random(60, 150));
  circle(xPos,random(-height/2, height/2), size);
  
}
