void setup(){
  size(1920, 1080, P3D);
  background(255);
 camera(0,0,-(height/2)/tan(PI*30/180),0,0,0,0,-1,0);
}

float[] dt = new float[5];

void draw(){ 

 Walker walk = new Walker(); 
 walk.RandomWalk(dt);
 walk.Render();

 for(int i = 0; i < dt.length; i++){
   if (i > 1){
  dt[i] += 0.01 * i;
  } 
  else dt[i] += 0.01;
 }
}  
