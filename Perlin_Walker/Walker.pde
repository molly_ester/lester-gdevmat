class Walker{
  float xPos, yPos;
  float size;
  
  void Render(){
    circle(xPos, yPos, size);
    noStroke();
  }
  
  void RandomWalk(float[] time){

    float x = noise(time[0]);
    float y = noise(time[1] + 100);
    xPos = map(x, 0, 1, Window.left, Window.right);
    yPos = map(y, 0, 1, Window.bottom, Window.top);
 
    fill(map(noise(time[2] + 69), 0f, 1f, 0f, 255f),
    map(noise(time[3] + 7001), 0f, 1f, 0f, 255f), 
    map(noise(time[4] + 680), 0f, 1f, 0f, 255f),
    map(noise(time[0] + 70), 0f, 1f, 60f, 150f));
    
    size = map(noise(time[1] + 500), 0, 1, 50, 200);
  }
  
}
