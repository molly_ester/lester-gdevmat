public class Vector2{
 public float x;
 public float y;
 
 Vector2(){
  this.x = 0;
  this.y = 0;
 }
  
 Vector2(float x, float y){
  this.x = x;
  this.y = y;
 }
 
 public Vector2 Add(Vector2 c){
  this.x  += c.x;
  this.y += c.y;
  return this;
   
 }
 
 public Vector2 Sub(Vector2 c){
  this.x -= c.x;
  this.y -= c.y;
  return this;
   
 }
 
 public Vector2 mult(float scalar){
   this.x *= scalar;
   this.y *= scalar;
   return this;
 }
 public float mag(){
   return sqrt((x*x) + (y * y));
 }
 
 public Vector2 div(float scalar){
   this.x /= scalar;
   this.y /= scalar;
   return this;
 }
 
 public Vector2 normalize(){
  float length = this.mag();
  
  if(length > 0){
    this.div(length);
  }
  
  return this;
 }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
}
