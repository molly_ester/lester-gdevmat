class Walker{
  Vector2 pos = new Vector2();
  Vector2 vel = new Vector2(9, 12);
  float size = 30;
  
  void Render(){
    circle(pos.x, pos.y, size);
    noStroke();
  }
  
  public void RandomWalk(float[] time){

    float x = noise(time[0]);
    float y = noise(time[1] + 100);
    pos.x = map(x, 0, 1, Window.left, Window.right);
    pos.y = map(y, 0, 1, Window.bottom, Window.top);
 
    fill(map(noise(time[2] + 69), 0f, 1f, 0f, 255f),
    map(noise(time[3] + 7001), 0f, 1f, 0f, 255f), 
    map(noise(time[4] + 680), 0f, 1f, 0f, 255f),
    map(noise(time[0] + 70), 0f, 1f, 60f, 150f));
    
    size = map(noise(time[1] + 500), 0, 1, 50, 200);
  }
  

  public void bouncingwalker(){

    pos.Add(vel);
    
    if (pos.x > Window.right || pos.x < Window.left){
      vel.x *= -1;
      fill(random(255), random(255), random(255));
    }
    if (pos.y > Window.top || pos.y < Window.bottom){
      vel.y *= -1;
      fill(random(255), random(255), random(255));
     }
  }
}
