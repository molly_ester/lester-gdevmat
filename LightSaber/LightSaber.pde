Vector2 pos = new Vector2(1,1);
Vector2 pos2 = new Vector2(1,1);

void setup(){
  size (1920, 1080, P3D);
  camera(0, 0, -(height/2.0) / tan(PI * 30.0f / 180.0), 0, 0, 0, 0, -1, 0);
  background(0.);
  pos.normalize();
    pos.mult(500);
    pos2.normalize();
  pos2.mult(50);
}


Vector2 vel = new Vector2(10, 12);
Walker walk = new Walker();
long prev;

Vector2 mousePos(){
  float x = mouseX - Window.windowWidth;
  float y = -(mouseY - Window.windowHeight);
  return new Vector2(x, y);
}

float dt = 69;
void draw(){ 
  clear();

  float map = map(noise(dt), 0, 1, 0, 2*PI);
  rotate(map);
  
  strokeWeight(15);
  stroke (map(noise(dt * 20), 0, 1, 0,255), map(noise(dt * 30), 0, 1, 0,255), map(noise(dt * 69), 0, 1, 0,255), 100);
  line(-pos.x,-pos.y,pos.x,pos.y);

   strokeWeight(6);
  stroke (map(noise(dt * 20), 0, 1, 0,255), map(noise(dt * 30), 0, 1, 0,255));
  line(-pos.x,-pos.y,pos.x,pos.y);
  
  strokeWeight(15);
  stroke (0,0,0);
  line(-pos2.x-5,-pos2.y-5,pos2.x+5, pos2.y+5);
  
   strokeWeight(15);
  stroke (192, 192,192);
  line(-pos2.x,-pos2.y,pos2.x, pos2.y);
  
   
  
  strokeWeight(5);
  stroke(255,0,0);
  line(0 + 10,0 + 5,10 + 10,10 + 5);
  
  
  dt += 0.001;
}
